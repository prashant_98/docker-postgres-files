# Docker-postgres-files

files and dependencies useful for the "postgres + docker" snippet mentioned here (https://gitlab.com/-/snippets/2022887).

_**--- STAGE 1 : SETTING UP ---**_

- Firstly make sure you have Docker installed.

- Once that is done make sure you have a Postgres Image downloaded.
     This can be done with the command  `"docker pull postgres"` (via Terminal)

- Once completed, it is now time to create a PostgreSQL based container with a new database (and  it's  corresponding DB Security Credentials). The reason we use 5416:5432 instead of 5432:5432 as that port is currently using an external DB (via pgAdmin). One can stick with 5432:5432 if it is vacant.
    - `"sudo docker run --name <YOUR_CHOICE_OF_CONTAINER_NAME> -p 127.0.0.1:5416:5432 -e POSTGRES_PASSWORD=<YOUR_CHOICE_OF_PASSWORD> -e POSTGRES_DB=<YOUR_CHOICE_OF_DB_NAME> -d postgres"`         


_**--- STAGE 2 : RESTORING DB --- (specific to the event model team)**_

- navigate to the directory you have your backup stored then 
    - `"cat dbbackup.sql | docker exec -i <YOUR_CHOICE_OF_CONTAINER_NAME> pg_restore -U postgres -d <YOUR_CHOICE_OF_DB_NAME>"`

- to test and see if it all works
	- run `"docker exec -i <YOUR_CHOICE_OF_CONTAINER_NAME> bash"` to run commands in the container (in an interactive fashion). 
	- `"psql -U postgres <YOUR_CHOICE_OF_DB_NAME>"` (in the bash of the container, which is essentially where you reach if the previous step is succesfully completed)


_**--- STAGE 3 (TO MAKE SURE IT IS ACCESSIBLE OUTSIDE THE CONTAINER)---**_

- Open a new terminal window and run the command (also make sure your container is running).
    - `"psql -h 127.0.0.1 -p 5416 -U postgres -d <YOUR_CHOICE_OF_DB_NAME>"`
    - you can now make your queries.
    - some shortcuts, if you want to see the tables your DB currently has simply type `"\dt+"`
    - list of databases = `"\l+"`
    - list of users = `"\du+"`
